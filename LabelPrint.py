import sys
from os import path
import pywintypes
from win32com.client import Dispatch

def printLabel(FFEid, FFEvar, FFEdev, NESadd, cosmGrade, distGrade, nidekRes, artefacts):
    curdir = None
    if getattr(sys, 'frozen', False):
    	curdir = path.dirname(sys.executable)
    else:
    	curdir = path.dirname(path.abspath(__file__))

    mylabel = path.join(curdir,'Focuss7.label')

    if not path.isfile(mylabel):
        print("no label file")
        sys.exit(1)
    try:
        labelCom = Dispatch('Dymo.DymoAddIn')
        labelText = Dispatch('Dymo.DymoLabels')
        isOpen = labelCom.Open(mylabel)
        selectPrinter = 'DYMO LabelWriter 450'
        labelCom.SelectPrinter(selectPrinter)

        labelText.SetField('FFE_ID', FFEid)
        labelText.SetField('FFE_VAR', FFEvar)
        labelText.SetField('FFE_DEV', FFEdev)
        labelText.SetField('NES_Add', NESadd)
        labelText.SetField('CosmeticGrade', cosmGrade)
        labelText.SetField('DistortionGrade', distGrade)
        if "Fail" in nidekRes:
            labelText.SetField('INFO', ("FAILED \n Drop Ball Only"))
        else:
           labelText.SetField('INFO', artefacts)

        labelCom.StartPrintJob()
        labelCom.Print2(1, False, 1)
        labelCom.EndPrintJob()

    except:
        print("print error")
        return