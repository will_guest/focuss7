#-------------------------------------------------------------------------------
# Name:        deltaCyl
#
# Author:      W Guest
#
# Created:     03/08/2015
# Copyright:   (c) wguest 2015
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import math

def calc_delta_cyl(sphereU, cylU, axisU, sphereA, cylA, axisA):

    matU1 = sphereU + cylU*(pow(math.sin(math.radians(axisU)),2))
    matU2 = -cylU*math.sin(math.radians(axisU))*math.cos(math.radians(axisU))
    matU3 = sphereU + cylU*(pow(math.cos(math.radians(axisU)),2))

    matA1 = sphereA + cylA*(pow(math.sin(math.radians(axisA)),2))
    matA2 = -cylA*math.sin(math.radians(axisA))*math.cos(math.radians(axisA))
    matA3 = sphereA + cylA*(pow(math.cos(math.radians(axisA)),2))

    delta1 = matA1 - matU1
    delta2 = matA2 - matU2
    delta3 = matA3 - matU3

    trace = delta1 + delta3
    deter = (delta1*delta3)-(delta2*delta2)

    cylD = round(-math.sqrt((trace*trace)-(4*deter)),2)
    sphD = round((trace-cylD)/2,2)
    if delta2 == 0:
        axsD = 90
    elif math.atan((sphD-delta1)/delta2) > 0:
        axsD = math.degrees(math.atan((sphD-delta1)/delta2))
    else:
        axsD = int(180 + math.degrees(math.atan((sphD-delta1)/delta2)))

    return (sphD, cylD, axsD)