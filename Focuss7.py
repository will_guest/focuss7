#-----------------------------------------------------------------------------#
# Name:        Focuss7                                                        #
# Purpose:     Streamlining the Cosmetic Inspection and Review Processes      #
#                                                                             #
# Author:      Will Guest                                                     #
#                                                                             #
# Created:     04-Jan-2016                                                    #
# Copyright:   (c) Will Guest 2016                                            #
#-----------------------------------------------------------------------------#
#!/usr/bin/env python3

import sys, csv

from PyQt4 import uic, Qt, QtCore, QtGui
from datetime import datetime
from os import path, getcwd

import Focuss7_rc
import GrabNidekData
from SQLcomms import focussDatabase
from deltaCyl import calc_delta_cyl
from LabelPrint import printLabel
from Report7 import RepWindow

guiFile = path.join(path.dirname(sys.argv[0]), 'Focuss7.ui')
form_class = uic.loadUiType(guiFile, False, '_rc')[0]


class MainWindow(QtGui.QMainWindow, form_class):

    def __init__(self, parent=None):
        # Initialise main window
        super(QtGui.QMainWindow, self).__init__(parent=None)
        self.setupUi(self)
        QtCore.pyqtRemoveInputHook()
        QtGui.QMainWindow.move(self, 500, 10)

        # Header Event handlers
        self.btnBatch.clicked.connect(self.getBatchNo)
        self.cmbOperator.currentIndexChanged.connect(self.selectOperator)
        self.btnClearPage.clicked.connect(self.reset_Meas_Tab)
        self.btnBatchComplete.clicked.connect(self.complete_batch)

        # Measurement Tab Event Handlers
        self.btnMeasure.clicked.connect(self.get_nidek_data)
        self.btnReset.clicked.connect(self.resetLists)
        self.btnConfirmData.clicked.connect(self.confirmData)

        self.slidePart.valueChanged.connect(lambda:
            self.linePart.setText(str("{:02}".format(self.slidePart.value()))))

        self.linePart.textChanged.connect(self.checkLineCursor)
        self.linePart.editingFinished.connect(self.updateSlidePart)
        self.slideCosm.sliderPressed.connect(self.updateLineCosm)
        self.slideCosm.valueChanged.connect(self.updateLineCosm)
        self.checkLeak.stateChanged.connect(self.setLeak)

        self.listCosm.itemChanged.connect(self.checkForSpecks)
        self.listDefects.itemChanged.connect(self.checkForSpecks)
        self.listArtefacts.itemChanged.connect(self.checkForSpecks)

        # Data Tab Event Handlers
        self.btnPrintLabel.clicked.connect(self.print_to_label)
        self.btnClearTables.clicked.connect(self.clear_data_tables)
        self.tblDataO.cellClicked.connect(lambda:
            self.update_data_tbl("tblDataC",
            self.tblDataO.item(self.tblDataO.currentRow(), 0).text()))
        self.tblDataC.cellClicked.connect(lambda:
            self.update_data_tbl("tblDataO",
            self.tblDataC.item(self.tblDataC.currentRow(), 0).text()))

        # Review Tab Event Handlers
        self.tabWidget.currentChanged.connect(self.selectOperator)
        self.listReview.currentRowChanged.connect(self.setButtonStates)
        self.btnRevUpdate.clicked.connect(self.updatePartDetails)
        self.btnRevRecycle.clicked.connect(self.revRecycle)

        self.btnCosmPass.clicked.connect(self.cosmPass)
        self.btnCosmFail.clicked.connect(self.cosmFail)
        self.btnDistPass.clicked.connect(self.distPass)
        self.btnDistFail.clicked.connect(self.distFail)

        self.cmbReviewBatches.currentIndexChanged.connect(self.get_modules_for_review)
        self.cmbBatches.currentIndexChanged.connect(self.show_batch_summary)
        self.btnPrintReport.clicked.connect(self.prepare_report)

        # Enable Hover Options for Widgets
        self.btnLogo.installEventFilter(self)

        # Assign Object Properties that tend to drift
        self.initUI()

    def initUI(self):
        self.tabWidget.setCurrentIndex(0)
        self.lblCosmRes.lower()
        self.lblDistRes.lower()
        self.listRevCode.lower()
        self.tblQuickView.horizontalHeader().show()
        self.tblDataO.horizontalHeader().show()
        self.tblDataC.horizontalHeader().show()

        configFile = [line.strip() for line in open("config.ini", 'r')]
        for i in range(len(configFile)):
            if "Reviewers" in configFile[i]:
                self.cmbOperator.addItems(configFile[i][10:].split(", "))
            if "Operators" in configFile[i]:
                self.cmbOperator.addItems(configFile[i][10:].split(", "))

    def eventFilter(self, object, event):
        MBP = QtCore.QEvent.MouseButtonPress
        if (event.type() == QtCore.QEvent.Enter and object is self.btnLogo):
            self.groupPageInfo.raise_()
        if (event.type() == QtCore.QEvent.Leave and object is self.btnLogo):
            self.groupPageInfo.lower()
        return QtGui.QWidget.eventFilter(self, object, event)

    def prompt(self, msg = None):
        msgBox = QtGui.QMessageBox(None)
        msgBox.setWindowFlags(QtCore.Qt.WindowStaysOnTopHint)
        msgBox.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        msgBox.setWindowTitle("Attention")
        msgBox.setText(msg)
        msgBox.exec_()

    def ask_yn(self, question = None):
        reply = QtGui.QMessageBox.question(self, 'Message',
                question, QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if      reply == QtGui.QMessageBox.Yes:     return 'Yes'
        elif    reply == QtGui.QMessageBox.No:      return 'No'

    def update_data_tbl(self, target, partNo):
        if partNo is not None:
            for row in range(eval("self." + target + ".rowCount()")):
                if eval("self." + target + ".item(" + str(row) + ",0).text()") == partNo:
                    eval("self." + target + ".setCurrentCell(" + str(row) + ",0)")


    #------------------ AUTOMATIC UPDATES ON 'MEASURE' TAB -------------------#

    def getBatchNo(self):
        batchInput = QtGui.QInputDialog(None)
        batchInput.setWindowTitle("FFE Batch No.")

        batchInput.setFixedSize(300,200)
        batchInput.setInputMode(1)
        batchInput.setIntRange(1, 9999)
        batchInput.setCancelButtonText("Cancel")
        checkDev = QtGui.QCheckBox(batchInput)
        checkDev.move(20, 60)
        checkDev.setText("Dev Build")

        okay = batchInput.exec()

        if okay == 1:
            batchNo = batchInput.intValue()
            self.btnBatch.setText("FM" + str("{:04}".format(batchNo)))

            if checkDev.checkState() == 2:
                (devNo, ok) = QtGui.QInputDialog.getInt(
                    self, "Attention", "Please Enter Dev. Build Reference",1,1,999)
                if ok:  self.lblDevNo.setText("DEV" + str("{:03}".format(devNo)))
            else:   self.lblDevNo.setText("")

        elif okay == 0:
            self.btnBatch.setText("Batch #")

    def checkForSpecks(self):
        speck = self.listCosm.findItems("Specks:",QtCore.Qt.MatchStartsWith)
        speckDef = self.listDefects.findItems("Specks",QtCore.Qt.MatchExactly)
        speckArt = self.listArtefacts.findItems("Specks",QtCore.Qt.MatchExactly)

        if len(speckDef) == 1:
            (noSpecks, ok) = QtGui.QInputDialog.getInt(self,
                                "Attention", "How Many Specks?", 1, 1, 99)
            speckDef[0].setText("Specks: " + str(noSpecks))

        elif len(speckArt) == 1:
            (noSpecks, ok) = QtGui.QInputDialog.getInt(self,
                                "Attention", "How Many Specks?", 1, 1, 99)
            speckArt[0].setText("Specks: " + str(noSpecks))

        elif len(speck) == 1:
            speck[0].setText("Specks")

        if (self.listDefects.count()) > 0 and (self.slideCosm.value() < 14):
            self.slideCosm.setValue(14)

        self.listCosm.sortItems()

    def checkLineCursor(self):
        if self.linePart.cursorPosition() == 2:
            self.updateSlidePart()

    def updateSlidePart(self):
        lineInt = int(self.linePart.text())
        if lineInt < 10:
            lineInt = str("{:02}".format(int(self.linePart.text())))
            self.linePart.setText(lineInt)
        self.slidePart.setValue(int(self.linePart.text()))

    def updateLineCosm(self):
        self.lineCosm.setText(str(hex(
                            self.slideCosm.value()).split('x')[1])[:].upper())

    def setLeak(self):
        if self.checkLeak.isChecked() == True:
            self.btnMeasure.setDisabled(True)
            self.slideCosm.setDisabled(True)
            self.slideDist.setDisabled(True)
        elif self.checkLeak.isChecked() == False:
            self.btnMeasure.setEnabled(True)
            self.slideCosm.setEnabled(True)
            self.slideDist.setEnabled(True)


    #---------------------------- RESET FUNCTIONS ----------------------------#

    def resetLists(self):
        while self.listDefects.count() > 0:
            self.listCosm.addItem(self.listDefects.item(0).text())
            self.listDefects.takeItem(0)
        while self.listArtefacts.count() > 0:
            self.listCosm.addItem(self.listArtefacts.item(0).text())
            self.listArtefacts.takeItem(0)
        self.checkForSpecks()

    def clear_data_tables(self):
        ans = self.ask_yn("Are you sure you want to do that?")
        if ans == "Yes":
            self.tblDataO.clearContents()
            self.tblDataO.setRowCount(0)
            self.tblDataC.clearContents()
            self.tblDataC.setRowCount(0)

    def reset_Meas_Tab(self):
        self.slidePart.setValue(0)
        self.checkLeak.setChecked(False)
        self.slideCosm.setValue(10)
        self.lineCosm.setText("")
        self.slideDist.setValue(1)
        self.tblQuickView.clearContents()
        self.lblDisp.setText("")

        self.resetLists()


    #---------------------------- NIDEK FUNCTIONS ----------------------------#

    def get_sphere_error(self, variant, sphere):
        if variant == '02' or variant == '16':      sphereError = sphere + 0.5
        elif variant == '06' or variant == '17':    sphereError = sphere + 2.5
        elif variant == '12' or variant == '18':    sphereError = sphere + 4.5
        return sphereError

    def tolerance_data(self, sphereError, cylinder, hpr, vpr, NESAdd):
        if sphereError > 1:     FailMode = 3
        elif cylinder < -1:     FailMode = 4
        elif hpr > 0.75:        FailMode = 6
        elif vpr > 0.5:         FailMode = 8
        elif NESAdd < 1.75:     FailMode = 11
        else:                   FailMode = 0

        if FailMode > 0:
            self.tblDataO.item(self.tblDataO.currentRow(),
                 FailMode).setTextColor(QtGui.QColor(255,125,125))
        return FailMode

    def choose_hand(self):
        msgBox =  QtGui.QMessageBox()
        msgBox.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        msgBox.setText('Is this a Left of a Right?')
        btnL = QtGui.QPushButton('Left')
        msgBox.addButton(btnL, QtGui.QMessageBox.YesRole)
        btnR = QtGui.QPushButton('Right')
        msgBox.addButton(btnR, QtGui.QMessageBox.NoRole)
        choice = msgBox.exec_()

        if choice == 0:     return ("L")
        elif choice == 1:   return ("R")

    def updateQuickView(self):
        try:
            currRow = self.tblDataO.currentRow()
        except:
            self.tblQuickView.clearContents()
            return

        recSph = QtGui.QTableWidgetItem(self.tblDataO.item(currRow,3).text())
        recCyl = QtGui.QTableWidgetItem(self.tblDataO.item(currRow,4).text())
        recAxs = QtGui.QTableWidgetItem(self.tblDataO.item(currRow,5).text())
        recAdd = QtGui.QTableWidgetItem(self.tblDataO.item(currRow,11).text())
        recRes = QtGui.QTableWidgetItem(self.tblDataO.item(currRow,12).text())

        self.tblQuickView.setItem(0, 0, recSph)
        self.tblQuickView.setItem(0, 1, recCyl)
        self.tblQuickView.setItem(0, 2, recAxs)
        self.tblQuickView.setItem(0, 3, recAdd)
        self.tblQuickView.setItem(0, 4, recRes)

        if self.tblQuickView.item(0, 4).text() == "Pass":
            self.tblQuickView.item(0, 4).setBackground(QtGui.QColor(0,135,0))
        elif self.tblQuickView.item(0, 4).text() == "Fail":
            self.tblQuickView.item(0, 4).setBackground(QtGui.QColor(135,0,0))

    def get_nidek_data(self):
        # Check for necessary part information
        if self.btnBatch.text() == "Batch #":
            self.prompt("No batch number chosen")
            return
        if self.cmbVar.currentText() == "Variant":
            self.prompt("Please select a variant from the list")
            return
        if self.cmbOperator.currentText() == "Operator":
            self.prompt("No operator selected - who are you?")
            return
        if self.slidePart.value() == 0:
            self.prompt("No part number selected")
            return

        # Check last entry for a match
        partNo = self.btnBatch.text() + "-" + self.linePart.text()
        lastRowO = self.tblDataO.rowCount() - 1

        # Grab Nidek Screen (COM port from config.ini file)
        try:
            configFile = [line.strip() for line in open("config.ini", 'r')]
            for i in range(len(configFile)):
                if "Nidek COMport" in configFile[i]:
                    getCOM = "COM" + str(configFile[i][-1:])
                    break

            (sphere, cylinder, axis, hpr,
                hPrDir, vpr, vPrDir, hand) = GrabNidekData.measure(getCOM)
        except:
            self.prompt("Connection to Nidek not made. Please check COM port.")
            return

        # Add row to optics table
        noRows = self.tblDataO.rowCount()
        self.tblDataO.insertRow(noRows)
        self.tblDataO.setCurrentCell(noRows, 0)
        currRow = self.tblDataO.currentRow()

        DataP = QtGui.QTableWidgetItem(partNo)
        DataV = QtGui.QTableWidgetItem(self.cmbVar.currentText())

        if hand == " ":
            DataH = QtGui.QTableWidgetItem(self.choose_hand())
        else:
            DataH = QtGui.QTableWidgetItem(str(hand))

        self.tblDataO.setItem(currRow, 0, DataP)
        self.tblDataO.setItem(currRow, 1, DataV)
        self.tblDataO.setItem(currRow, 2, DataH)

        # Set Table Items and Update Form
        Data0 = QtGui.QTableWidgetItem(str(sphere))
        Data1 = QtGui.QTableWidgetItem(str(cylinder))
        Data2 = QtGui.QTableWidgetItem(str(axis))
        Data3 = QtGui.QTableWidgetItem(str(hpr))
        Data4 = QtGui.QTableWidgetItem(str(hPrDir))
        Data5 = QtGui.QTableWidgetItem(str(vpr))
        Data6 = QtGui.QTableWidgetItem(str(vPrDir))

        self.tblDataO.setItem(currRow,3, Data0)
        self.tblDataO.setItem(currRow,4, Data1)
        self.tblDataO.setItem(currRow,5, Data2)
        self.tblDataO.setItem(currRow,6, Data3)
        self.tblDataO.setItem(currRow,7, Data4)
        self.tblDataO.setItem(currRow,8, Data5)
        self.tblDataO.setItem(currRow,9, Data6)

        # Grab Actuated Data and populate 'NES Add' column
        NESUnact = sphere + (cylinder/2)
        self.prompt("Please actuate, replace on fixture and then press 'OK'")

        (sphAct, cylAct, axsAct, hprDirAct,
            hprAct, vprDirAct, vprAct, hand)  = GrabNidekData.measure(getCOM)

        # Calc Delta Cyl
        (deltaSph, deltaCyl, deltaAxs) = calc_delta_cyl(sphere, cylinder, axis,
                                                        sphAct, cylAct, axsAct)
        Data7 = QtGui.QTableWidgetItem(str(deltaCyl))
        self.tblDataO.setItem(currRow,10, Data7)

        NESAct = sphAct + (cylAct / 2)
        NESAdd = round(NESAct - NESUnact,2)

        Data8 = QtGui.QTableWidgetItem(str(NESAdd))
        self.tblDataO.setItem(currRow, 11, Data8)

        # Label as Pass / Fail
        try:
            sphereError = self.get_sphere_error(self.cmbVar.currentText(),
                                                            float(sphere))
        except:
            self.prompt("Please choose a lens variant")
            return

        failMode = self.tolerance_data(sphereError, cylinder, hpr, vpr, NESAdd)
        if failMode != 0:
            Data9 = QtGui.QTableWidgetItem("Fail")
            self.tblDataO.setItem(currRow, 12, Data9)
        else:
            Data9 = QtGui.QTableWidgetItem("Pass")
            self.tblDataO.setItem(currRow, 12, Data9)
        self.updateQuickView()


    #--------------------- CONFIRM BUTTON (MEASURE TAB) ----------------------#

    def getCosmGrade(self):
        if self.slideCosm.value() < 13:
            return ("Pass", "n/a")
        elif self.slideCosm.value() == 13:
            return ("Review", "")
        elif self.slideCosm.value() > 13:
            return ("Fail", "n/a")

    def getDistGrade(self):
        if self.slideDist.value() == 1:
            return ("Pass", "n/a")
        elif self.slideDist.value() == 2:
            return ("Review", "")
        elif self.slideDist.value() == 3:
            return ("Fail", "n/a")

    def add_row(self):
        # Add row in cosmetic table and make active
        noRows = self.tblDataC.rowCount()
        self.tblDataC.insertRow(noRows)
        self.tblDataC.setCurrentCell(noRows, 0)

    def confirmLeak(self):

        self.add_row()
        currRow = self.tblDataC.currentRow()

        # Set nominal info for failed module
        partNo = self.btnBatch.text() + "-" + self.linePart.text()
        devRef = self.lblDevNo.text()
        variantC = self.cmbVar.currentText()
        handC = self.choose_hand()

        focussDatabase.upsert_FFEinfo_record(partNo, devRef, variantC, handC, "Recycle")

        self.tblDataC.setItem(currRow, 0, QtGui.QTableWidgetItem(partNo))
        self.tblDataC.setItem(currRow, 1, QtGui.QTableWidgetItem(variantC))
        self.tblDataC.setItem(currRow, 2, QtGui.QTableWidgetItem(handC))
        self.tblDataC.setItem(currRow, 4, QtGui.QTableWidgetItem("Leak"))
        self.tblDataC.setItem(currRow, 6, QtGui.QTableWidgetItem("Leak"))
        self.tblDataC.setItem(currRow, 15, QtGui.QTableWidgetItem("n/a"))
        self.tblDataC.setItem(currRow, 16, QtGui.QTableWidgetItem("Recycle"))
        self.tblDataC.setItem(currRow, 17, QtGui.QTableWidgetItem("Leak"))

        self.update_cosmetic_info()
        self.reset_Meas_Tab()

    def op_colour_grade(self, NESadd, nidekResult):
        currRow = self.tblDataC.currentRow()

        # If nidek, cosmetic or distortion results are 'fail',
        # or there is less than 1.75 add, recycle FFE
        if (self.tblDataC.item(currRow, 4).text() == "Fail" or
            self.tblDataC.item(currRow, 4).text() == "Leak" or
            self.tblDataC.item(currRow, 6).text() == "Fail" or
            nidekResult == "Fail" or NESadd < 1.75):
                self.tblDataC.setItem(currRow, 16,
                                            QtGui.QTableWidgetItem("Recycle"))
                self.tblDataC.setItem(currRow, 17,
                                            QtGui.QTableWidgetItem("Fail"))

        # If cosmetic and distortion results are 'pass',
        # and there is more than 2.36 add, put in green box
        elif (self.tblDataC.item(currRow, 4).text() == "Pass" and
            self.tblDataC.item(currRow, 6).text() == "Pass" and
            NESadd >= 2.36):
                self.tblDataC.setItem(currRow, 16,
                                            QtGui.QTableWidgetItem("Green"))
                self.tblDataC.setItem(currRow, 17,
                                            QtGui.QTableWidgetItem("Pass"))

        # If cosmetic and distortion results are 'pass',
        # and there is between 2.00 and 2.36 add, put in orange box
        elif (self.tblDataC.item(currRow, 4).text() == "Pass" and
            self.tblDataC.item(currRow, 6).text() == "Pass" and
            2.00 <= NESadd < 2.36):
                self.tblDataC.setItem(currRow, 16,
                                            QtGui.QTableWidgetItem("Orange"))
                self.tblDataC.setItem(currRow, 17,
                                            QtGui.QTableWidgetItem("Pass"))

        # otherwise tag for review (no disposition yet)
        else:
            self.tblDataC.setItem(currRow, 16, QtGui.QTableWidgetItem("In Review"))
            self.tblDataC.setItem(currRow, 17, QtGui.QTableWidgetItem(""))

    def confirmData(self):
        # Check for necessary part information
        if self.btnBatch.text() == "Batch #":
            self.prompt("No batch number chosen")
            return
        if self.cmbVar.currentText() == "Variant":
            self.prompt("Please select a variant from the list")
            return
        if self.cmbOperator.currentText() == "Operator":
            self.prompt("No operator selected - who are you?")
            return
        if self.slidePart.value() == 0:
            self.prompt("No part number selected")
            return

        partNo = self.btnBatch.text() + "-" + self.linePart.text()
        devRef = self.lblDevNo.text()
        variant = self.cmbVar.currentText()

        lastRowO = self.tblDataO.rowCount() - 1
        lastRowC = self.tblDataC.rowCount() - 1

        # If module is leaking, update .csv accordingly
        if self.checkLeak.isChecked() == True:
            self.confirmLeak()
            return

        # Check that Nidek measurement has been done
        try:
            if self.tblDataO.item(lastRowO, 0).text() != partNo:
                self.prompt("Nidek measurement not done")
                return
        except:
            self.prompt("Error getting Nidek data")
            return

        # Check for cosmetic grading
        if self.lineCosm.text() == "":
            self.prompt ("Please enter a cosmetic grading")
            return

        # Check there aren't too many entries for the table
        if self.listDefects.item(2) != None:
            self.prompt("Error. Please limit the number of defects to 2")
            return
        elif self.listArtefacts.item(5) != None:
            self.prompt("Error. Please limit the number of artefacts to 5")
            return

        dataP =  QtGui.QTableWidgetItem(partNo)
        dataV =  QtGui.QTableWidgetItem(self.cmbVar.currentText())
        hand = self.tblDataO.item(lastRowO, 2).text()
        handItem = QtGui.QTableWidgetItem(hand)

        # ^   Error trapping complete  ^ #
        # |----------------------------| #
        # v Grab interface, store data v #

        # Add and populate a row in cosmetic table
        self.add_row()

        currRow = self.tblDataC.currentRow()
        self.tblDataC.setItem(currRow, 0, dataP)
        self.tblDataC.setItem(currRow, 1, dataV)
        self.tblDataC.setItem(currRow, 2, handItem)

        # Update Cosmetic Grade in tblDataC and/or Give Results
        (cosmRes, cosmRev) = self.getCosmGrade()
        cosmGrade =  QtGui.QTableWidgetItem(self.lineCosm.text())
        cosmResult = QtGui.QTableWidgetItem(cosmRes)
        cosmReview = QtGui.QTableWidgetItem(cosmRev)
        self.tblDataC.setItem(currRow, 3, cosmGrade)
        self.tblDataC.setItem(currRow, 4, cosmResult)
        self.tblDataC.setItem(currRow, 5, cosmReview)

        # Update Distortion Grade in tblDataC and/or Give Results
        (distRes, distRev) = self.getDistGrade()
        distResult = QtGui.QTableWidgetItem(distRes)
        distReview = QtGui.QTableWidgetItem(distRev)
        self.tblDataC.setItem(currRow, 6, distResult)
        self.tblDataC.setItem(currRow, 7, distReview)

        # Put all artefacts & defects into cosmetic data table
        for x in range(len(self.listDefects)):
            itemDef = QtGui.QTableWidgetItem(self.listDefects.item(x).text())
            self.tblDataC.setItem(currRow, x + 8, itemDef)

        artefactList = []

        for x in range(len(self.listArtefacts)):
            itemArt = QtGui.QTableWidgetItem(self.listArtefacts.item(x).text())
            self.tblDataC.setItem(currRow, x + 10, itemArt)
            artefactList.append(self.listArtefacts.item(x).text())
        artefacts = (', '.join(artefactList))

        # Bring Nidek result into Cosmetics table
        nidekRes = self.tblDataO.item(lastRowO, 12).text()
        self.tblDataC.setItem(currRow, 15, QtGui.QTableWidgetItem(nidekRes))

        # Get Add value and print label
        FFEhand = self.tblDataC.item(currRow, 2).text()
        nidekAdd = float(self.tblQuickView.item(0, 3).text())
        nidekRes = (self.tblQuickView.item(0, 4).text())

        lblPartNo = ("*" + partNo + "*")
        printLabel(lblPartNo, self.cmbVar.currentText() + "-" + FFEhand,
                    devRef, nidekAdd, self.lineCosm.text(), distRes, nidekRes, artefacts)

        # Assign a box location, based on add, and cosm. and dist. results
        # (recycle if nidek result is 'Fail')
        self.op_colour_grade(nidekAdd, nidekRes)
        colourGrade = self.tblDataC.item(currRow, 16).text()

        # Update Database tables
        try:
            focussDatabase.upsert_FFEinfo_record(partNo, devRef, variant, hand, colourGrade)
            self.update_optics_info()
            self.update_cosmetic_info()
        except:
            self.prompt("Error saving information, please contact system administrator")
            return

        # Reset form objects for next module
        self.reset_Meas_Tab()


    #------------------------------- SEND TO DB -------------------------------#

    def update_optics_info(self):
        partNo = self.btnBatch.text() + "-" + self.linePart.text()
        self.update_data_tbl("tblDataO", partNo)

        now = datetime.today()
        dateStamp = now.strftime('%d %b %Y')
        timeStamp = now.strftime('%H:%M')
        opticalData = []

        opticalData.append(partNo)
        opticalData.append(dateStamp)
        opticalData.append(timeStamp)
        opticalData.append(self.cmbOperator.currentText())

        for column in range(3, self.tblDataO.columnCount()):
            item = self.tblDataO.item(self.tblDataO.currentRow(), column)
            if item is not None:
                opticalData.append(item.text())
            else:
                opticalData.append('-')

        focussDatabase.upsert_7Nidek_record(partNo, tuple(opticalData))

    def update_cosmetic_info(self):
        partNo = self.btnBatch.text() + "-" + self.linePart.text()
        self.update_data_tbl("tblDataC", partNo)
        now = datetime.today()
        dateStamp = now.strftime('%d %b %Y')
        timeStamp = now.strftime('%H:%M')
        cosmeticData = []

        cosmeticData.append(partNo)
        cosmeticData.append(dateStamp)
        cosmeticData.append(timeStamp)
        cosmeticData.append(self.cmbOperator.currentText())

        for column in range(3, self.tblDataC.columnCount()):
            item = self.tblDataC.item(self.tblDataC.currentRow(), column)
            if item is not None:
                cosmeticData.append(item.text())
            else:
                cosmeticData.append('-')
        focussDatabase.upsert_7Cosmetics_record(partNo, tuple(cosmeticData))

    def complete_batch(self):

        # Reset fields and tables ready for next batch
        self.btnBatch.setText("Batch #")
        self.lblDevNo.setText("")
        self.cmbVar.setCurrentIndex(0)
        self.cmbOperator.setCurrentIndex(0)

        self.reset_Meas_Tab()


    #------------------------- REVIEW TAB: FUNCTIONS -------------------------#

    def selectOperator(self):
        configFile = [line.strip() for line in open("config.ini", 'r')]
        for i in range(len(configFile)):
            if "Reviewers" in configFile[i]:
                reviewers = str(configFile[i][10:])
            if "Operators" in configFile[i]:
                operators = str(configFile[i][10:])

        if (self.tabWidget.currentIndex() == 2) and (
            self.cmbOperator.currentText() in reviewers):
            self.get_batches_for_review()
            self.get_batches_for_report()

        elif (self.tabWidget.currentIndex() == 2) and (
            self.cmbOperator.currentText() in operators):
            self.listReview.clear()
            self.listRevCode.clear()
            for i in range (1, (self.cmbReviewBatches.count())):
                self.cmbReviewBatches.removeItem(1)
            self.get_batches_for_report()
        else:
            self.listReview.clear()
            self.listRevCode.clear()
            for i in range (1, (self.cmbReviewBatches.count())):
                self.cmbReviewBatches.removeItem(1)
            for k in range (1, (self.cmbBatches.count())):
                self.cmbBatches.removeItem(1)

            self.get_batches_for_report()

    def get_batches_for_review(self):

        # Set contents of review drop-down list
        batch4rev = focussDatabase.get_batches_for_review()
        for i in range (1, (self.cmbReviewBatches.count())):
            self.cmbReviewBatches.removeItem(1)
        for j in range(len(batch4rev)):
            self.cmbReviewBatches.addItem(batch4rev[j][0])

    def get_modules_for_review(self):
        self.listReview.clear()
        self.listRevCode.clear()

        if "Select" in self.cmbReviewBatches.currentText():
            return

        # Get FFEs that have blank fields for either 'SME' column
        mod4rev = focussDatabase.get_modules_for_review(self.cmbReviewBatches.currentText())

        if len(mod4rev) > 0:
            for x in range(len(mod4rev)):
                if "Fail" in mod4rev[x][1]:
                    focussDatabase.update_FFEinfo_location(mod4rev[x][0], "Recycle", "cosmFail")
                    continue
                elif "Fail" in mod4rev[x][2]:
                    focussDatabase.update_FFEinfo_location(mod4rev[x][0], "Recycle", "distFail")
                    continue

                # Identify which aspects need reviewing
                elif "Review" in mod4rev[x][1] and "Pass" in mod4rev[x][2]:
                    revCode = 0     #: only cosmetic review
                elif "Pass" in mod4rev[x][1] and "Review" in mod4rev[x][2]:
                    revCode = 1     #: only distortion review
                elif "Review" in mod4rev[x][1] and "Review" in mod4rev[x][2]:
                    revCode = 2     #: both for review
                self.listReview.addItem(mod4rev[x][0])
                self.listRevCode.addItem(str(revCode))
        else:
            self.get_batches_for_review()

    def get_batches_for_report(self):
        # Set contents of report drop-down list
        sortedBatches = list(focussDatabase.get_batch_list())

        for k in range (1, (self.cmbBatches.count())):
            self.cmbBatches.removeItem(1)
        for l in range(len(sortedBatches)):
            self.cmbBatches.addItem(str(sortedBatches[l][0]))

    def cosmPass(self):
        self.btnCosmFail.setChecked(False)
        self.checkDisp()
    def cosmFail(self):
        self.btnCosmPass.setChecked(False)
        self.checkDisp()
    def distPass(self):
        self.btnDistFail.setChecked(False)
        self.checkDisp()
    def distFail(self):
        self.btnDistPass.setChecked(False)
        self.checkDisp()

    def checkDisp(self):
        if (self.btnCosmPass.isEnabled() == True and
            self.btnDistPass.isEnabled() == True):
            if (    self.btnCosmPass.isChecked() == True and
                    self.btnDistPass.isChecked() == True):
                self.lblDisp.setText("Pass")
            elif (  self.btnCosmPass.isChecked() == True and
                    self.btnDistFail.isChecked() == True):
                self.lblDisp.setText("Fail")
            elif (  self.btnCosmFail.isChecked() == True and
                    self.btnDistPass.isChecked() == True):
                self.lblDisp.setText("Fail")
            elif (  self.btnCosmFail.isChecked() == True and
                    self.btnDistFail.isChecked() == True):
                self.lblDisp.setText("Fail")
            else:   self.lblDisp.setText("")
        elif(   self.btnCosmPass.isEnabled() == True and
                self.btnDistPass.isEnabled() == False):
            if (self.btnCosmPass.isChecked() == True):
                self.lblDisp.setText("Pass")
            elif (self.btnCosmFail.isChecked() == True):
                self.lblDisp.setText("Fail")
            else:   self.lblDisp.setText("")
        elif(   self.btnCosmPass.isEnabled() == False and
                self.btnDistPass.isEnabled() == True):
            if (self.btnDistPass.isChecked() == True):
                self.lblDisp.setText("Pass")
            elif (self.btnDistFail.isChecked() == True):
                self.lblDisp.setText("Fail")
            else:   self.lblDisp.setText("")
        else:   return

    def setButtonStates(self):
        try:
            caseNo = str(self.listRevCode.item(
                        self.listReview.currentRow()).text())
        except:
            self.btnCosmPass.setEnabled(False)
            self.btnCosmFail.setEnabled(False)
            self.btnDistPass.setEnabled(False)
            self.btnDistFail.setEnabled(False)

            self.btnCosmPass.setChecked(False)
            self.btnCosmFail.setChecked(False)
            self.btnDistPass.setChecked(False)
            self.btnDistFail.setChecked(False)
            self.lblDisp.setText("")
            return

        self.listRevCode.currentRow() == self.listReview.currentRow()

        if caseNo == '0':
            self.btnCosmPass.setEnabled(True)
            self.btnCosmFail.setEnabled(True)
            self.btnDistPass.setEnabled(False)
            self.btnDistFail.setEnabled(False)

            self.btnCosmPass.setChecked(False)
            self.btnCosmFail.setChecked(False)
            self.btnDistPass.setChecked(False)
            self.btnDistFail.setChecked(False)
            self.lblDisp.setText("")
        elif caseNo == '1':
            self.btnCosmPass.setEnabled(False)
            self.btnCosmFail.setEnabled(False)
            self.btnDistPass.setEnabled(True)
            self.btnDistFail.setEnabled(True)

            self.btnCosmPass.setChecked(False)
            self.btnCosmFail.setChecked(False)
            self.btnDistPass.setChecked(False)
            self.btnDistFail.setChecked(False)
            self.lblDisp.setText("")
        elif caseNo == '2':
            self.btnCosmPass.setEnabled(True)
            self.btnCosmFail.setEnabled(True)
            self.btnDistPass.setEnabled(True)
            self.btnDistFail.setEnabled(True)

            self.btnCosmPass.setChecked(False)
            self.btnCosmFail.setChecked(False)
            self.btnDistPass.setChecked(False)
            self.btnDistFail.setChecked(False)
            self.lblDisp.setText("")

    def revRecycle(self):
        try:    caseNo = str(self.listRevCode.item(
                            self.listReview.currentRow()).text())
        except: return

        ans = self.ask_yn("Are you sure you want to recycle this FFE?")
        if ans == 'Yes':
            self.lblDisp.setText("Leak")
            self.updatePartDetails()
        else:
            return

    def updatePartDetails(self):
        if self.lblDisp.text() == "":
            return
        partNo = self.listReview.currentItem().text()
        try:    caseNo = str(self.listRevCode.item(
                        self.listReview.currentRow()).text())
        except: return

        if self.btnCosmPass.isChecked()     == True:    cosmSME = "Pass"
        elif self.btnCosmFail.isChecked()   == True:    cosmSME = "Fail"

        if self.btnDistPass.isChecked()     == True:    distSME = "Pass"
        elif self.btnDistFail.isChecked()   == True:    distSME = "Fail"

        if self.lblDisp.text() == "":
            self.prompt("Cannot update; not enough information")
            return
        elif self.lblDisp.text() == "Leak":
            cosmSME = "Fail"
            distSME = "Fail"
            colourGrade = "Recycle"
        elif self.lblDisp.text() == "Pass":  colourGrade = "Orange"
        elif self.lblDisp.text() == "Fail":  colourGrade = "Red"

        if   caseNo == '0': reviewData = cosmSME
        elif caseNo == '1': reviewData = distSME
        elif caseNo == '2': reviewData = (cosmSME, distSME)

        focussDatabase.update_7Cosmetics(
                partNo, caseNo, reviewData, colourGrade, self.lblDisp.text())

        if self.listReview.count() < 1:
            self.get_batches_for_review()
        else:
            self.get_modules_for_review()

    def show_batch_summary(self):

        selectedBatch = self.cmbBatches.currentText()
        if selectedBatch == "Select":
            for x in [0,1]:
                for y in [1,3,5]:
                    self.tblBatchSum.setItem(x, y, QtGui.QTableWidgetItem(""))
            self.lblDevRef.setText("")
            return

        allMod, passMod, failMod = focussDatabase.get_batch_overview(selectedBatch)
        noM = len(allMod)
        noP = len(passMod)      #could be done in a better way
        noF = len(failMod)
        date = allMod[0][1].strftime('%d %b %y')
        variant = allMod[0][2]
        devRef = allMod[0][3]

        batchNum = QtGui.QTableWidgetItem(selectedBatch)
        batchDat = QtGui.QTableWidgetItem(str(date))
        batchVar = QtGui.QTableWidgetItem(str(variant))
        batchNoM = QtGui.QTableWidgetItem(str(noM))
        batchNoP = QtGui.QTableWidgetItem(str(noP))
        batchNoF = QtGui.QTableWidgetItem(str(noF))

        self.tblBatchSum.setItem(0,1, batchNum)
        self.tblBatchSum.setItem(0,3, batchDat)
        self.tblBatchSum.setItem(0,5, batchVar)
        self.tblBatchSum.setItem(1,1, batchNoM)
        self.tblBatchSum.setItem(1,3, batchNoP)
        self.tblBatchSum.setItem(1,5, batchNoF)
        self.lblDevRef.setText(devRef)


    #-------------------------- PRINTING -------------------------------------#

    def print_to_label(self):
        currRowC = self.tblDataC.currentRow()
        currRowO = self.tblDataO.currentRow()

        if currRowC == -1:
            self.prompt("Error: No record selected")
            return

        ans = self.ask_yn("Do you want to print a label?")

        if ans == 'Yes':
            partNo = self.tblDataC.item(currRowC, 0).text()
            variant = self.tblDataC.item(currRowC, 1).text()
            hand = self.tblDataC.item(currRowC, 2).text()
            NESadd = self.tblDataO.item(currRowO, 11).text()
            cosmGrade = self.tblDataC.item(currRowC, 3).text()
            distRes = self.tblDataC.item(currRowC, 6).text()
            nidekRes = self.tblDataC.item(currRowC, 15).text()
            artefacts = ""

            artefactList = []

            for x in range(len(self.listArtefacts)):
                itemArt = QtGui.QTableWidgetItem(self.listArtefacts.item(x).text())
                self.tblDataC.setItem(currRow, x + 10, itemArt)
                artefactList.append(self.listArtefacts.item(x).text())
            artefacts = (', '.join(artefactList))

            try:
                printLabel(partNo, variant +  "-" + hand,
                    nidekAdd, cosmGrade, distRes, nidekRes, artefacts)
            except:
                self.prompt("An error occurred while trying to print")
                return

    def prepare_report(self):
        # Check for necessary info
        batchNo = self.cmbBatches.currentText()
        dateToday = datetime.today().strftime('%d %b %Y')

        if batchNo == "Select":     return

        # Check all modules have been reviewed
        batchNo = self.tblBatchSum.item(0, 1).text()
        devRef = self.lblDevRef.text()
        batchDate = self.tblBatchSum.item(0, 3).text()
        batchVar = self.tblBatchSum.item(0, 5).text()

        if "DEV" in devRef:
            batchRef = (self.cmbBatches.currentText() + " / " + devRef)
        else:
            batchRef = batchNo

        NoFFEs = int(self.tblBatchSum.item(1, 1).text())
        NoPass = int(self.tblBatchSum.item(1, 3).text())
        NoFail = int(self.tblBatchSum.item(1, 5).text())

        if NoPass + NoFail !=  NoFFEs:
            self.prompt("Please review modules before printing report")
            return

        inputVals = focussDatabase.get_batch_details(batchNo)
        pages = (len(inputVals)//26) + 1

        for i in range(pages):
            firsti = (i*25)
            lasti  = ((i+1)*25)
            pageStr = ("Page " + str(i+1) + " of " + str(pages))
            self.show_report(batchRef, batchDate, batchVar, inputVals[firsti:lasti], pageStr)

    def show_report(self, batchNo, batchDate, batchVar, inputVals, pageStr):
        #Initialise Report Window
        self.rWin = RepWindow()
        self.rWin.__init__()
        self.rWin.populate_table(batchNo, batchDate, batchVar, inputVals, pageStr)
        self.rWin.show()

        self.rWin.print_to_file(pageStr)

        reply = QtGui.QMessageBox.question(self.rWin, 'Question',
                "Print a hard copy?", QtGui.QMessageBox.Yes, QtGui.QMessageBox.No)
        if reply == QtGui.QMessageBox.Yes:
            self.rWin.print_report()

if __name__ == '__main__':
    app = None
    app = QtGui.QApplication(sys.argv)
    app.setStyle("plastique")
    myWindow = MainWindow()
    myWindow.show()
    sys.exit(app.exec_())