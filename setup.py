from distutils.core import setup
import py2exe, sys

sys.argv.append('py2exe')

class Target:
    def __init__(self, **kw):
        self.__dict__.update(kw)
        self.version = "1.0.1"
        self.company_name = "Adlens"
        self.copyright = "Copyright (c)2016 Will Guest"
        self.product_name = "Focuss7"

target = Target(
    description = "Focuss FFE Inspection Assistant",
    script = "Focuss7.py",
    dest_base = "Focuss7")

setup(
    options = {'py2exe':{'bundle_files': 3,
    'compressed': False, "includes":["sip", "decimal"]}},
    zipfile=None,
    windows = [target]
)
