#-------------------------------------------------------------------------------
# Name:        module1
# Purpose:
#
# Author:      wguest
#
# Created:     03-02-2016
# Copyright:   (c) wguest 2016
# Licence:     <your licence>
#-------------------------------------------------------------------------------

import sys
from inspect import getsourcefile
from os import path
from PyQt4 import uic, Qt, QtCore, QtGui

guiFile = path.join(path.dirname(sys.argv[0]), 'Report7.ui')
form_report = uic.loadUiType(guiFile, True)[0]

class RepWindow(QtGui.QMainWindow, form_report):
    def __init__(self, parent=None):
        # Initialise main window
        super(QtGui.QMainWindow,self).__init__(parent=None)
        self.setupUi(self)
        Adlogo = QtGui.QPixmap(path.dirname(getsourcefile(lambda:0)) + '\Images\Adlogo-b.png')
        self.lblAdlogo.setPixmap(Adlogo)
        self.btnPrint.clicked.connect(self.print_to_file)

    def set_colour(self, value):
        if "Pass" in value.text():
            return (QtGui.QColor(155,255,155))
        elif "Review" in value.text():
            return (QtGui.QColor(255,189,155)) #red
        if "Fail" in value.text():
            return (QtGui.QColor(255,155,155)) #red=(255,125,125)
        else:
            return (QtGui.QColor('lightGray'))

    def populate_table(self, batchNo, dateToday, SKU, reportData, page):
        self.lblBatch.setText(batchNo)
        self.lblBatchCode.setText('*' + batchNo[:6] + '*')
        self.lblDate.setText(dateToday)
        self.lblSKU.setText(SKU)
        self.lblPage.setText(page)

        if "DEV" in batchNo:
            font = QtGui.QFont("Franklin Gothic Demi Cond", 14)
            self.lblBatch.setFont(font)

        # Populate table with array
        for row in range(len(reportData)):
            noRows = self.tblReport.rowCount()
            self.tblReport.insertRow(noRows)
            for item in range(len(reportData[row])):
                tableItem = QtGui.QTableWidgetItem(str(reportData[row][item]))
                tableItem.setBackgroundColor(self.set_colour(tableItem))
                self.tblReport.setItem(row, item, tableItem)

    def print_to_file(self, pageStr):
        printer = QtGui.QPrinter()
        directory = "T:/MEASUREMENTS/Focuss7/pdf/"
        filename = self.lblBatch.text() + pageStr + ".pdf"

        printer.setOutputFormat(QtGui.QPrinter.PdfFormat)
        printer.setOutputFileName(directory + filename)
        printer.setPageMargins(7,10,7,10,0)
        printer.setResolution(108)

        p = QtGui.QPainter(printer)
        QtGui.QMainWindow.move(self, 500, 50)
        self.centralwidget.render(p)
        del p, printer

        #self.print_report()

    def print_report(self):
        printer = QtGui.QPrinter()
        printer.setPageMargins(7,10,7,10,0)
        printer.setResolution(108)
        painter = QtGui.QPainter(printer)
        document = self.centralwidget

        dialog = QtGui.QPrintDialog()
        if dialog.exec_() == QtGui.QDialog.Accepted:
            document.render(painter)
            del painter