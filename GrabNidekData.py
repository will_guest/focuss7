import serial, time

def measure(comPort):

    try:
        ser = serial.Serial(port=comPort, baudrate=9600,
                            parity = serial.PARITY_ODD,
                            stopbits=serial.STOPBITS_ONE,
                            bytesize = serial.EIGHTBITS, timeout=1)
    except: 
        return
            
    ser.close()
    ser.open()
    RSD = b"\x01\x43\x4C\x4D\x02\x52\x53\x44\x17\x04\x0D"
    CLR = b"\x01\x43\x4C\x4D\x02\x43\x4C\x17\x04\x0D"

    ser.write(RSD)
    NidekData = str(ser.read(77)[44:])

    #Clear Screen
    time.sleep(0.1)
    ser.write(CLR)
    time.sleep(0.1)
    ser.close()
    del ser

    sphere = float(NidekData[2:8])
    cylinder = float(NidekData[8:14])
    axis = int(NidekData[14:17])
    hand = str(NidekData[22:23])
    hPrism = float(NidekData[23:28])
    hPrDir = str(NidekData[28:29])
    vPrism = float(NidekData[35:40])
    vPrDir = str(NidekData[40:41])

    return (sphere, cylinder, axis, hPrism, hPrDir, vPrism, vPrDir, hand)