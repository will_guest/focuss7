#-------------------------------------------------------------------------------
# Name:        Focuss7 SQLcomms
# Purpose:
#
# Author:      Will Guest
#
# Created:     24/01/2012
# Copyright:   (c) wguest 2012
# Licence:     <your licence>
#-------------------------------------------------------------------------------
import pyodbc

class focussDatabase():

    def init_conn():
        return pyodbc.connect('Driver={SQL Server Native Client 11.0};'
                       'Server=tcp:zcxd3jvhs4.database.windows.net,1433;'
                       'Database=Focuss;'
                       'Uid=nasuni_admin;Pwd=Specs4@ll;Encrypt=yes;'
                       'TrustServerCertificate=no;Connection Timeout=30;')

    #-------------------- ADDING / UPDATING RECORDS ------------------------#

    def upsert_FFEinfo_record(partNo, devRef, variant, hand, location):
        connection = focussDatabase.init_conn()
        cursor = connection.cursor()
        SQLchk = cursor.execute("SELECT TOP 1 PartNo "
                                "FROM FFEInfo WHERE PartNo = '" + partNo + "'")
        recChk = SQLchk.fetchone()

        if recChk is None:
            SQLadd = ("Insert INTO FFEinfo "
                    "(PartNo, DevRef, Variant, Hand, Location) "
                    "Values (?,?,?,?,?)")
            Values = (partNo, devRef, variant, hand, location)
            cursor.execute(SQLadd, Values)
            connection.commit()
        else:
            SQLupd = ("UPDATE FFEinfo "
                    "SET PartNo = ?, DevRef = ?, "
                    "Variant = ?, Hand = ?, "
                    "Location = ? "
                    "WHERE PartNo = '" + partNo + "'")
            Values = (partNo, devRef, variant, hand, location)
            cursor.execute(SQLupd, Values)
        connection.commit()
        connection.close()

    def upsert_7Nidek_record(partNo, opticalData):
        connection = focussDatabase.init_conn()
        cursor = connection.cursor()
        SQLchk = cursor.execute("SELECT TOP 1 PartNo "
                                "FROM [7Nidek] "
                                "WHERE PartNo = '" + partNo + "'")
        recChk = SQLchk.fetchone()

        if recChk is None:
            valuesExpr = ((len(opticalData)-1)*'?,' + '?')
            SQLadd = ("Insert INTO [7Nidek] "
                    "(PartNo, Date, Time, Operator, "
                    "Sphere, Cylinder, Axis, H_Prism, H_PrismDir, "
                    "V_Prism, V_PrismDir, deltaCyl, NES_Add, Result) "
                    "Values (" + valuesExpr + ")")
            cursor.execute(SQLadd, opticalData[:])
        else:
            SQLupd = ("UPDATE [7Nidek] "
                    "SET PartNo = ?, Date = ?, Time = ?, Operator = ?, "
                    "Sphere = ?, Cylinder = ?, Axis = ?, H_Prism = ?, H_PrismDir = ?, "
                    "V_Prism = ?, V_PrismDir = ?, deltaCyl = ?, NES_Add = ?, Result = ? "
                    "WHERE PartNo = '" + partNo + "'")
            Values = (opticalData)
            cursor.execute(SQLupd, Values)
        connection.commit()
        connection.close()

    def upsert_7Cosmetics_record(partNo, cosmeticData):
        connection = focussDatabase.init_conn()
        cursor = connection.cursor()
        SQLchk = cursor.execute("SELECT TOP 1 PartNo "
                                "FROM [7Cosmetics] "
                                "WHERE PartNo = '" + partNo + "'")
        recChk = SQLchk.fetchone()

        if recChk is None:
            valuesExpr = ((len(cosmeticData)-1)*'?,' + '?')
            SQLadd = ("Insert INTO [7Cosmetics] "
                    "(PartNo, Date, Time, Operator, "
                    "Cosmetic_Grade, Cosmetic_Result, Cosmetic_SME, "
                    "Distortion_Result, Distortion_SME, "
                    "Defect_1, Defect_2, "
                    "Artefact_1, Artefact_2, Artefact_3, Artefact_4, Artefact_5, "
                    "Nidek_Result, Colour_Grade, Disposition) "
                    "Values (" + valuesExpr + ")")
            cursor.execute(SQLadd, cosmeticData)
        else:
            SQLupd = ("UPDATE [7Cosmetics] "
                    "SET PartNo = ?, Date = ?, Time = ?, Operator = ?, "
                    "Cosmetic_Grade = ?, Cosmetic_Result = ?, Cosmetic_SME = ?, "
                    "Distortion_Result = ?, Distortion_SME = ?, "
                    "Defect_1 = ?, Defect_2 = ?, "
                    "Artefact_1 = ?, Artefact_2 = ?, Artefact_3 = ?, Artefact_4 = ?, Artefact_5 = ?, "
                    "Nidek_Result = ?, Colour_Grade = ?, Disposition = ? "
                    "WHERE PartNo = '" + partNo + "'")
            Values = (cosmeticData)
            cursor.execute(SQLupd, Values)
        connection.commit()
        connection.close()

    #------------------------- REVIEWING RECORDS ---------------------------#

    def get_batches_for_review():
        connection = focussDatabase.init_conn()
        cursor = connection.cursor()
        SQLget = ("SELECT DISTINCT LEFT(PartNo, 6) "
                    "FROM [7Cosmetics] "
                    "WHERE Cosmetic_SME = '' "
                    "OR Distortion_SME = ''")
        cursor.execute(SQLget)
        results = cursor.fetchall()
        connection.commit()
        connection.close()
        return results

    def get_modules_for_review(selectedBatch):
        connection = focussDatabase.init_conn()
        cursor = connection.cursor()
        SQLget = ("SELECT DISTINCT PartNo, Cosmetic_Result, Distortion_Result "
                    "FROM [7Cosmetics] "
                    "WHERE PartNo LIKE '" + selectedBatch + "%' "
                    "AND (Cosmetic_SME = '' "
                    "OR Distortion_SME = '')")
        cursor.execute(SQLget)
        results = cursor.fetchall()
        connection.commit()
        connection.close()
        return results

    def update_FFEinfo_location(partNo, location, failMode):
        connection = focussDatabase.init_conn()
        cursor = connection.cursor()
        SQLupd1 = ("UPDATE FFEinfo "
                "SET Location = ? "
                "WHERE PartNo = ?")
        Values1 = (location, partNo)
        cursor.execute(SQLupd1, Values1)

        if failMode == "cosmFail":
            SQLupd2 = ("UPDATE [7Cosmetics] "
                        "SET [Distortion_SME] = ? "
                        "WHERE PartNo = ?")
            Values2 = ("n/a", partNo)
        elif failMode == "distFail":
            SQLupd2 = ("UPDATE [7Cosmetics] "
                        "SET [Cosmetic_SME] = ? "
                        "WHERE PartNo = ?")
            Values2 = ("n/a", partNo)
        cursor.execute(SQLupd2, Values2)

        connection.commit()
        connection.close()

    def update_7Cosmetics(partNo, caseNo, reviewData, colourGrade, disp):
        connection = focussDatabase.init_conn()
        cursor = connection.cursor()

        if caseNo == '0':
            SQLupdate = ("UPDATE [7Cosmetics] "
                    "SET [Cosmetic_SME] = ?, "
                    "[Colour_Grade] = ?, "
                    "[Disposition] = ? "
                    "WHERE [PartNo] = ?")
            Values = (reviewData, colourGrade, disp, partNo)

        elif caseNo == '1':
            SQLupdate = ("UPDATE [7Cosmetics] "
                    "SET [Distortion_SME] = ?, "
                    "[Colour_Grade] = ?, "
                    "[Disposition] = ? "
                    "WHERE [PartNo] = ?")
            Values = (reviewData, colourGrade, disp, partNo)

        elif caseNo == '2':
            SQLupdate = ("UPDATE [7Cosmetics] "
                    "SET [Cosmetic_SME] = ?, "
                    "[Distortion_SME] = ?, "
                    "[Colour_Grade] = ?, "
                    "[Disposition] = ? "
                    "WHERE [PartNo] = ?")
            Values = (reviewData[0], reviewData[1], colourGrade, disp, partNo)

        SQLupd2 = ("UPDATE FFEinfo "
                    "SET Location = ? "
                    "WHERE PartNo = ?")
        Values2 = (colourGrade, partNo)

        cursor.execute(SQLupdate, Values)
        cursor.execute(SQLupd2, Values2)

        connection.commit()
        connection.close()

    #-------------------------- PRINTING REPORTS ---------------------------#

    def get_batch_list():
        connection = focussDatabase.init_conn()
        cursor = connection.cursor()
        SQL = ("SELECT DISTINCT LEFT(PartNo, 6), Date FROM [7Cosmetics] "
                "ORDER BY Date DESC")
        cursor.execute(SQL)
        results = cursor.fetchall()
        connection.commit()
        connection.close()
        if results != None: return list(results)
        else:               return ""

    def get_batch_overview(selectedBatch):
        connection = focussDatabase.init_conn()
        cursor = connection.cursor()
        SQLall = ("SELECT [7Cosmetics].PartNo, Date, "
                    "FFEinfo.Variant, FFEinfo.DevRef "
                    "FROM [7Cosmetics] "
                    "INNER JOIN FFEinfo "
                    "ON [FFEinfo].PartNo = [7Cosmetics].PartNo "
                    "WHERE [7Cosmetics].PartNo LIKE '" + selectedBatch + "%'")
        cursor.execute(SQLall)
        allMod = cursor.fetchall()

        SQLpass = ("SELECT PartNo FROM [7Cosmetics] "
                    "WHERE PartNo LIKE '" + selectedBatch + "%' "
                    "AND Disposition = 'Pass'")
        cursor.execute(SQLpass)
        passMod = cursor.fetchall()

        SQLfail = ("SELECT PartNo FROM [7Cosmetics] "
                    "WHERE PartNo LIKE '" + selectedBatch + "%' "
                    "AND (Disposition = 'Fail' "
                    "OR Disposition = 'Leak')")
        cursor.execute(SQLfail)
        failMod = cursor.fetchall()

        connection.commit()
        connection.close()
        return allMod, passMod, failMod

    def get_batch_details(selectedBatch):
        connection = focussDatabase.init_conn()
        cursor = connection.cursor()
        SQLget = ("SELECT [7Cosmetics].PartNo, FFEinfo.Hand, Nidek_Result, "
                    "Cosmetic_Result, Cosmetic_SME, "
                    "Distortion_Result, Distortion_SME, Disposition "
                    "FROM [7Cosmetics] "
                    "INNER JOIN FFEinfo "
                    "ON [FFEinfo].[PartNo] = [7Cosmetics].[PartNo] "
                    "WHERE [7Cosmetics].PartNo LIKE '" + selectedBatch + "%'")
        cursor.execute(SQLget)
        results = cursor.fetchall()
        connection.commit()
        connection.close()
        return results

